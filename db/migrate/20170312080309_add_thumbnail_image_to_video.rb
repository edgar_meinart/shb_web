class AddThumbnailImageToVideo < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :thumbnail_url, :string
    add_column :videos, :thumbnail_path, :string
  end
end
