class CreateCameras < ActiveRecord::Migration[5.0]
  def change
    create_table :cameras do |t|
      t.string :name
      t.decimal :lat, precision: 15, scale: 10
      t.decimal :lng,  precision: 15, scale: 10
      t.string :address
      t.string :token
      t.string :feed_url
      t.string :status

      t.timestamps
    end
  end
end
