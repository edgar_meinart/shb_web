class AddPreviewLinkToCamera < ActiveRecord::Migration[5.0]
  def change
    add_column :cameras, :preview_link, :string
  end
end
