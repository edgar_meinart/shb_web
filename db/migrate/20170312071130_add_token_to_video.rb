class AddTokenToVideo < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :token, :string
    add_column :videos, :url_to_video, :string
    add_index :videos, :token
  end
end
