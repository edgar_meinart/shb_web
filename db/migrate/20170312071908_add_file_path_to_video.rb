class AddFilePathToVideo < ActiveRecord::Migration[5.0]
  def change
    add_column :videos, :file_path, :string
  end
end
