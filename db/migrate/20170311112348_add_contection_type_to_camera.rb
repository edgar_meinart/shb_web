class AddContectionTypeToCamera < ActiveRecord::Migration[5.0]
  def change
    add_column :cameras, :connection_type, :string
  end
end
