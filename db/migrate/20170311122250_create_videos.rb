class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.integer :camera_id
      t.datetime :recording_date
      t.string :file_name

      t.timestamps
    end
  end
end
