class AddS3AttributeToCameras < ActiveRecord::Migration[5.0]
  def change
    add_column :cameras, :s3_attribute, :string
    add_index :cameras, :s3_attribute
  end
end
