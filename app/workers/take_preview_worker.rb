class TakePreviewWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(video_id)
    video = Video.find video_id
    Video::Thumbnail.call video: video
  end

end
