class S3SyncWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    Video::Fetch.call
  end
end
