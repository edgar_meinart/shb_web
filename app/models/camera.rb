class Camera < ApplicationRecord
  CONNECTION_TYPES = [
    CONNECTION_TYPE_EXTERNAL = 'external',
    CONNECTION_TYPE_INTERNAL = 'internal',
    CONNECTION_TYPE_SIMPLE_PLAY_FROM_URL = 'play_from_url',
  ]

  has_many :videos

  validates :connection_type, presence: true
  validates :name, presence: true
  validates :s3_attribute, uniqueness: true, allow_blank: true
  after_initialize :init

  def self.s3_mapping
    where.not(s3_attribute: nil).pluck(:s3_attribute, :id).to_h
  end

  def init
    if new_record?
      self.token ||= Digest::SHA1.hexdigest "#{Time.now.to_i.to_s}#{feed_url}"
      self.preview_link ||= '/preview/default.png'
      self.connection_type ||= CONNECTION_TYPE_SIMPLE_PLAY_FROM_URL
    end
  end

end

