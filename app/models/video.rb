class Video < ApplicationRecord

  default_scope { order(recording_date: :desc) }

  belongs_to :camera
  validates :token, :url_to_video, uniqueness: true
  after_commit :take_thumbnail, on: :create

  after_initialize :init


  def take_thumbnail
    TakePreviewWorker.perform_async id
  end

  def init
    if new_record?
      self.thumbnail_url ||= '/preview/default.png'
    end
  end

end

