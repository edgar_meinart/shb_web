class Video::Fetch
  include Service
  PATTERN = /\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}/

  def call
    load_credentials
    fetch_remote! { |remote_obj| proceed_file(remote_obj) }
  end

  def fetch_remote!
    s3 = Aws::S3::Resource.new
    s3.bucket('sec-cam').objects.each do |object|
      yield object
    end
  end

  def proceed_file s3_obj
    token = Digest::SHA1.hexdigest s3_obj.key
    return if Video.exists?(token: token)
    camera_id = camera_id_for_object(s3_obj.key)
    return if camera_id.blank?
    File.open(file_name_for(token), 'wb') do |file|
      file.write s3_obj.get.body.read
    end
    Video.new do |v|
      v.camera_id = camera_id
      v.file_path = file_name_for(token)
      v.recording_date = recording_date_from(s3_obj.key) || Date.today
      v.token = token
      v.url_to_video = url_to_video_for(token)
    end.save!
    p "#{s3_obj.key} processed"
  end

  def load_credentials
    Aws.config.update({
      region: CONFIG['s3']['region'],
      credentials: Aws::Credentials.new(CONFIG['s3']['access_key'], CONFIG['s3']['secret_access_key'])
    })
  end

  def file_name_for(token, extension = "mp4")
    [Rails.root, "/public/history/", "#{token}.#{extension}"].join
  end

  def url_to_video_for(token, extension = "mp4")
    "/history/#{token}.#{extension}"
  end

  def camera_id_for_object(video_name)
    @mapping ||= Camera.s3_mapping
    res = {}
    @mapping.each do |key, value|
      res[value] = video_name.include?(key)
    end
    res.delete_if { |_, value| !value }
    return nil if res.blank?
    res.keys.first
  end

  def recording_date_from(video_name)
    result = PATTERN.match video_name
    return if !result
    DateTime.strptime result[0].to_s, "%Y_%m_%d_%H_%M"
  end
end
