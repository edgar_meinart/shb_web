class Video::Thumbnail
  include Service
  attribute :video, Video

  def call
    movie = FFMPEG::Movie.new(video.file_path)
    if movie.screenshot(file_path, resolution: '320x240')
      video.update! thumbnail_url: url_to_preview, thumbnail_path: file_path
    end
  end

  def file_path
    [Rails.root, "/public/preview/", "#{video.token}.png"].join
  end

  def url_to_preview
    "/preview/#{video.token}.png"
  end
end
