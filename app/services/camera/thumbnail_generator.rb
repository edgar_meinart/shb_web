class Camera::ThumbnailGenerator
  include Service
  attribute :camera, Camera

  def call
    if generate_thumbnail!
      camera.update!(preview_link: "/preview/#{camera.token}.gif") if File.exists?(tmp_url)
    end
  end

  def generate_thumbnail!
    system(ffmeg_command)
  end

  def ffmeg_command
    @ffmeg_command ||= "ffmpeg -i https://stream-ire-alfa.dropcam.com/nexus_aac/62e262de78014fdf91de8350874785de/playlist.m3u8 -s 320x240 -t 5 #{camera.token}.gif"
  end

  def tmp_url
    [Rails.root, "/public/preview/", "#{camera.token}.gif"].join
  end
end
