module Service
  extend ActiveSupport::Concern

  included do
    include Virtus.model(strict: true)

    def self.call(*args)
      service_class.new(*args).call
    end

    def self.service_class
      self
    end
  end
end
