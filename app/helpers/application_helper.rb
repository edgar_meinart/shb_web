module ApplicationHelper

  def active_for name
    params[:action].to_sym == name ? 'active' : ''
  end
end
