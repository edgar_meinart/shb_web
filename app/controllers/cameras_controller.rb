class CamerasController < ApplicationController

  def new
    @resource = Camera.new
  end

  def create
    @resource = Camera.new camera_params
    if @resource.valid?
      @resource.save
      redirect_to @resource
    else
      render action: :new
    end
  end

  def show
    load_camera!
  end

  def history
    load_camera!
    @resources = @resource.videos.page(params[:page] || 1).per_page(10)
  end

  def play_history
    load_camera!
    load_video!
  end

  private

    def load_camera!
      @resource ||= Camera.find(params[:camera_id] || params[:id])
    end

    def load_video!
      @video = Video.find params[:video_id]
    end

    def camera_params
      params.require(:camera).permit(:name, :address, :connection_type, :feed_url, :token, :s3_attribute)
    end

end
