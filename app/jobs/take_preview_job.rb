class TakePreviewJob < ApplicationJob
  queue_as :default

  def perform(camera)
    Camera::ThumbnailGenerator.call(camera: camera)
  end

end
