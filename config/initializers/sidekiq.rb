#initializers/sidekiq.rb
schedule_file = "config/schedule.yml"

Sidekiq::Logging.logger.level = Logger::WARN

namespaces = {
  production: "shb_production_#{Rails.env}",
  default:    "shb_#{Rails.env}",
}

Sidekiq.configure_server do |config|
  config.redis = { namespace: namespaces.fetch(Rails.env.to_sym, namespaces[:default]) }
end

Sidekiq.configure_client do |config|
  config.redis = { namespace: namespaces.fetch(Rails.env.to_sym, namespaces[:default]) }
end

Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
