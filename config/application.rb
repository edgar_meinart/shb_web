require 'yaml'
require 'csv'
conf_file_name = File.expand_path('../config.yml', __FILE__)
raise RuntimeError, "config.yml doesn't exist" unless File.exists? conf_file_name
CONFIG = YAML::load_file(conf_file_name)[ENV['RAILS_ENV'] || "development"]


require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ShbWeb
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.autoload_paths += %W(#{config.root}/app/services)
    config.autoload_paths += %W(#{config.root}/app/workers)
  end
end
