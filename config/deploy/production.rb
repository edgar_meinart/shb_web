server 'proton2894.cloudapp.net', user: 'shb_web', roles: %w{web app db}
set :deploy_to, '/home/shb_web/app'
set :rails_env, 'production'
set :rvm_ruby_version, 'ruby-2.4.0@shb_web'
