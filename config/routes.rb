require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web => '/sidekiq'
  root to: 'home#index'

  get 'discover', to: 'discover#index'

  resources :cameras do
    get 'history', to: 'cameras#history'
    get 'history/:video_id', to: 'cameras#play_history', as: :play_video

  end

end
