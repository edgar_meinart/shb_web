# config valid only for current version of Capistrano
lock "3.8.0"

set :application, "shb_web"
set :repo_url, 'git@bitbucket.org:edgar_meinart/shb_web.git'

# Default branch is :master
set :branch, :master

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, false

# Default value for :linked_files is []
append :linked_files, "config/database.yml", "config/secrets.yml", "config/cable.yml", "config/puma.rb", "config/config.yml"

# Default value for linked_dirs is []

append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "config/cert", "public/preview", "public/history"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
